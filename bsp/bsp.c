/**
 * /bsp/bsp.c
 *
 * Copyright (C) 2017 HinsShum
 *
 * bsp.c is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*---------- includes ----------*/
#include "bsp.h"

/* function */
bool sysclock_init_div1(void)
{
    CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);
    return TRUE;
}

/* ms delay when system clock is 16M */
void bsp_delay_ms(uint16_t ms)
{
     volatile uint16_t i,j,z;
    
    for(z = 0; z < 1; z++) {    
        for(i = 0; i < ms; i++) {
            for(j = 0; j < 2560; j++);
        }
    }
}

void bsp_system_reset(void)
{
    asm("JPF $8000");
    while(TRUE);
}
