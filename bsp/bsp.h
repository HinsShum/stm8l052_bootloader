/**
 * /bsp/bsp.h
 *
 * Copyright (C) 2017 HinsShum
 *
 * bsp.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __BSP_H
#define __BSP_H

/* include */
#include <stm8l15x.h>

/* prototypes */
extern bool sysclock_init_div1(void);
extern void bsp_delay_ms(uint16_t ms);
extern void bsp_system_reset(void);

#endif /* __BSP_H */
