/**
 * /bsp/bsp_flash.c
 *
 * Copyright (C) 2017 HinsShum
 *
 * bsp_flash.c is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*---------- includes ----------*/
#include "bsp_flash.h"
#include "bsp.h"
#include <ctype.h>
#include <string.h>

/* function */
bool bsp_flash_init(void)
{
    FLASH_DeInit();
    FLASH_SetProgrammingTime(FLASH_ProgramTime_TProg);

    return TRUE;
}

bool bsp_flash_deinit(void)
{
    FLASH_DeInit();

    return TRUE;
}

bool bsp_flash_write(uint32_t start, uint8_t *src, uint16_t length)
{
    bool retval = TRUE;

    sim();  /* disalbe interrupt */
    FLASH_Unlock(FLASH_MemType_Program);
#if 0
    /* Promgram byte */
    while(length--) {
        FLASH_ProgramByte(start++, *src++);
    }
#endif
#if 1
    /* Promgram block */
    uint16_t block_start = (start - FLASH_PROGRAM_START_PHYSICAL_ADDRESS) / FLASH_BLOCK_SIZE;
    uint16_t block_end = (start + length - FLASH_PROGRAM_START_PHYSICAL_ADDRESS) / FLASH_BLOCK_SIZE;
    uint16_t block_current = block_start;
    uint16_t remain_length = (start + length) % FLASH_BLOCK_SIZE;
    for(; block_current < block_end; block_current++) {
        FLASH_ProgramBlock(block_current, FLASH_MemType_Program, FLASH_ProgramMode_Fast, src);
        src += FLASH_BLOCK_SIZE;
        if(FLASH_Status_Successful_Operation != FLASH_WaitForLastOperation(FLASH_MemType_Program)) {
            retval = FALSE;
            break;
        }
    }
    if(retval == TRUE && remain_length) {
        start = start + length - remain_length;
        while(remain_length--) {
            FLASH_ProgramByte(start++, *src++);
        }
    }
#endif
    FLASH_Lock(FLASH_MemType_Program);
    rim();  /* enable interrupt */

    return retval;
}

bool bsp_flash_erase_block(uint16_t blocknum)
{
    FLASH_Status_TypeDef operate_status = FLASH_Status_Write_Protection_Error;

    sim();  /* disalbe interrupt */
    FLASH_Unlock(FLASH_MemType_Program);
#if 0
    /* erase flash byte */
    for(uint16_t i = 0; i < FLASH_BLOCK_SIZE; i++) {
        FLASH_EraseByte(blocknum * FLASH_BLOCK_SIZE + FLASH_PROGRAM_START_PHYSICAL_ADDRESS + i);
    }
#endif
#if 1
    /* 这段代码编译会产生警告，因为 FLASH_EraseBlock() 在RAM中运行，但是在函数中试图访问FLASH中的代码 
     * 暂时未找到解决办法
     */
    FLASH_EraseBlock(blocknum, FLASH_MemType_Program);
    operate_status = FLASH_WaitForLastOperation(FLASH_MemType_Program);
#endif
    FLASH_Lock(FLASH_MemType_Program);
    rim();  /* enable interrupt */

    return ((operate_status == FLASH_Status_Successful_Operation) ? TRUE : FALSE);
}

#ifndef SYS_FLASH_ID_AND_KEY_ADDR
#define SYS_FLASH_ID_AND_KEY_ADDR  (0x00A7F0)
#endif /* SYS_FLASH_ID_AND_KEY_ADDR  */

bool bsp_flash_get_id_key(uint8_t *pid, uint8_t *pkey)
{
    char *default_key_id = "DRY00004"; 
    char *pt = (char *)SYS_FLASH_ID_AND_KEY_ADDR;
    char *tmp = pt;

    for(uint8_t i = 0; i < 16; i++) {
        if(!isalnum(pt[i])) {
            tmp = default_key_id;
            break;
        }
    }
    strncpy((char *)pid, tmp, 8);
    pid[8] = '\0';
    tmp = (tmp != default_key_id) ? (pt + 8) : tmp;
    strncpy((char *)pkey, tmp, 8);
    pkey[8] = '\0';

    return TRUE;
}
