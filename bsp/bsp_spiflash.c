/**
 * @bsp_spiflash.c
 *
 * @copyright This file creat by rensuiyi ,all right reserve!
 * 
 * @author rensuyi
 * 
 * @date 2017/7/18 16:29:02
 */
#include "bsp_spiflash.h"
#include "w25qxx.h"

/*
 *  SPI: SPI1
 *  
 *  CS   ->PB4
 *  MISO ->PB5
 *  SCK  ->PB6
 *  MOSI ->PB7  
 */

static bool bsp_spiflash_cs_ctl(bool on) {
  if (on) {
    GPIO_ResetBits(GPIOB, GPIO_Pin_4);
  }else{
    GPIO_SetBits(GPIOB, GPIO_Pin_4);
  }
  return TRUE;
}


static int32_t bsp_spiflash_register_init(void) {

  /* enable the clk */
  CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);

  /* set the mosi miso and sck pin level */
  GPIO_ExternalPullUpConfig(GPIOB, 
                            GPIO_Pin_5 |GPIO_Pin_6| GPIO_Pin_7,
                            ENABLE);

  GPIO_Init(GPIOB, GPIO_Pin_4, GPIO_Mode_Out_PP_High_Slow);

  /* cs high */
  bsp_spiflash_cs_ctl(FALSE);

  /* eanble the spi */
  SPI_Init(SPI1, SPI_FirstBit_MSB, SPI_BaudRatePrescaler_4, SPI_Mode_Master,
           SPI_CPOL_High, SPI_CPHA_2Edge, SPI_Direction_2Lines_FullDuplex,
           SPI_NSS_Soft, 0x07);

  /* Enable SPI  */
  SPI_Cmd(SPI1, ENABLE);

  return SL_EOK;
}



static uint8_t bsp_spiflash_xfer(uint8_t dat) {
  /* Loop while DR register in not emplty */
  while (SPI_GetFlagStatus(SPI1, SPI_FLAG_TXE) == RESET);
  /* Send byte through the SPI peripheral */
  SPI_SendData(SPI1, dat);
  /* Wait to receive a byte */
  while (SPI_GetFlagStatus(SPI1, SPI_FLAG_RXNE) == RESET);
  /* Return the byte read from the SPI bus */
  return SPI_ReceiveData(SPI1);
}


bool bsp_spiflash_init(void) {
  bsp_spiflash_register_init();

  g_w25qxx.cs_ctl = bsp_spiflash_cs_ctl;
  g_w25qxx.xfer = bsp_spiflash_xfer;

  return TRUE;
}
