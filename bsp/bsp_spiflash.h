/**
 * @file bsp_spiflash.h
 *  
 * @brief 
 * 
 *        
 *
 * @copyright This file creat by rensuiyi ,all right reserve!
 * 
 * @author rensuyi
 * 
 * @date 2017/7/18 16:28:53
 */
#ifndef __BSP_SPIFLASH_H__
#define __BSP_SPIFLASH_H__

#ifdef __cpluscplus
extern "c"
{
#endif /* __cpluscplus */

/* include */
#include <stm8l15x.h>
#include "errno.h"

extern bool bsp_spiflash_init(void);

#ifdef __cpluscplus
}
#endif /* __cpluscplus */
#endif /* __BSP_SPIFLASH_H__ */
