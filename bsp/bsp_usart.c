/**
 * /bsp/bsp_usart.c
 *
 * Copyright (C) 2017 HinsShum
 *
 * bsp_usart.c is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*---------- includes ----------*/
#include "bsp_usart.h"
#include "printk.h"
#include "user_serial.h"
#include "simplefifo.h"
#include "platform.h"

/* prototype */
static void bsp_usart_register(void);

bool bsp_usart_init(void)
{
    CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);
    GPIO_Init(GPIOC, GPIO_Pin_2, GPIO_Mode_In_PU_No_IT);
    GPIO_Init(GPIOC, GPIO_Pin_3, GPIO_Mode_Out_PP_High_Fast);
    USART_DeInit(USART1);
    USART_Init(USART1, 115200, USART_WordLength_8b, \
               USART_StopBits_1, USART_Parity_No, \
               (USART_Mode_TypeDef)(USART_Mode_Tx | USART_Mode_Rx));
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_Cmd(USART1, ENABLE);
    bsp_usart_register();

    return TRUE;
}

bool bsp_usart_deinit(void)
{
    CLK_PeripheralClockConfig(CLK_Peripheral_USART1, DISABLE);
    USART_DeInit(USART1);
    USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
    USART_Cmd(USART1, DISABLE);

    return TRUE;
}

#ifndef RELEASE_PUBLIC
static void bsp_usart_putc(uint8_t ch)
{
    if(ch == '\n') {
        USART_SendData8(USART1, '\r');
        while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    }
    USART_SendData8(USART1, ch);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
}

static uint16_t bsp_usart_puts(const uint8_t *str)
{
    uint16_t length = 0;

    while(str[length] && !(length == U16_MAX)) {
        bsp_usart_putc(str[length++]);
    }

    return length;
}
#endif

static uint16_t bsp_usart_write(uint8_t *src, uint16_t length)
{
    do {
        USART_SendData8(USART1, *src++);
        while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    } while(--length);

    return length;
}

static void bsp_usart_register(void)
{
#ifndef RELEASE_PUBLIC
    g_printk_struct.puts = bsp_usart_puts;
#endif
    g_user_serial.serial_write = bsp_usart_write;
}

bool isr_usart1_rx_handler(uint8_t ch, uint32_t s_vector)
{
    simplefifo_write(&g_platform.uart_fifo, &ch, 1);

    return TRUE;
}
