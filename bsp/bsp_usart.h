/**
 * /bsp/bsp_usart.h
 *
 * Copyright (C) 2017 HinsShum
 *
 * bsp_usart.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __BSP_USART_H
#define __BSP_USART_H

/* include */
#include <stm8l15x.h>

/* prototypes */
extern bool bsp_usart_init(void);
extern bool bsp_usart_deinit(void);
extern uint16_t bsp_usart_puts(const uint8_t *str);

#endif /* __BSP_USART_H */
