/**
* @file errno.h
*  
* @brief this is the system hook file ,used to initial the user 
*        task and user hardware
*
* @copyright This file creat by rensuiyi ,all right reserve!
* 
* @author rensuyi
* 
* @date 2015/4/20 10:29:09
*/
#ifndef __ERRNO_H__
#define __ERRNO_H__

#ifdef __cpluscplus
extern "c" 
{
#endif /* __cpluscplus */

#define  SL_OK                   (0)
#define  SL_EOK                  (0)
#define  SL_ERROR                (-1)
#define  SL_E_POINT_NONE         (-2)
#define  SL_E_TIME_OUT           (-3)
#define  SL_E_BUSY               (-4)
#define  SL_E_NO_MEM             (-5)
#define  SL_E_WRONG_ARGS         (-6)
#define  SL_E_WRONG_CRC          (-7)


#define  SL_E_DEVICE_IDEL        (-31)
#define  SL_E_DEVICE_POWER_OFF   (-32)

#define  SL_E_USER(x)            (x-100)

#ifdef __cpluscplus
}
#endif /* __cpluscplus */
#endif /* __ERRNO_H__ */
