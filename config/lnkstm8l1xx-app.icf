/////////////////////////////////////////////////////////////////
//      Example ILINK command file for
//      STM8 IAR C/C++ Compiler and Assembler.
//
//      Copyright 2017 HinsShum.
//
//      $Revision: 1623 $
//
/////////////////////////////////////////////////////////////////

/*-Symbol-*/
define symbol __intvec_start__      = 0x000000;
define symbol __region_TINY_start__ = 0x000000;
define symbol __region_TINY_end__   = 0x0000FF;
define symbol __region_ROM_start__  = 0x00C000;
define symbol __region_ROM_end__    = 0x00FFFF;
define symbol __region_RAM_start__  = 0x000000;
define symbol __region_RAM_end__    = 0x0007FF;

/*-Memory Regions-*/
define memory mem with size = 16M;
define region TinyData   = mem:[from __region_TINY_start__ to __region_TINY_end__];
define region ROM_region = mem:[from __region_ROM_start__ to __region_ROM_end__];
define region RAM_region = mem:[from __region_RAM_start__ to __region_RAM_end__];

/////////////////////////////////////////////////////////////////

define block CSTACK     with size    = _CSTACK_SIZE, alignment = 1 {};
define block HEAP       with size    = _HEAP_SIZE,   alignment = 1 {};
define block INTVEC     with size    = 0x80,         alignment = 4 { ro section .intvec };
define block INTVECMEM  with size    = 0x80,         alignment = 4 {};

initialize by copy { rw section .*.textrw, rw data, zi };
do not initialize { rw section .vregs, rw section .noinit };

// Keep block
keep { block INTVECMEM };

// Placement
place at start of ROM_region    { block INTVEC };
place in ROM_region             { ro code, ro data };     // includes ro code and data

place at address mem: __intvec_start__ { block INTVECMEM };
place in TinyData               { rw section .vregs };
place in RAM_region             { rw code, rw data, rw section .*.textrw };
place in RAM_region             { zi };
place in RAM_region             { block HEAP };
place at end of RAM_region      { block CSTACK };
/////////////////////////////////////////////////////////////////
