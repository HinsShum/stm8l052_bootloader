/**
 * /config/options.h
 *
 * Copyright (C) 2017 HinsShum
 *
 * platform.c is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __OPTIONS_H__
#define __OPTIONS_H__

#ifdef __cpluscplus
extern "c"
{
#endif /* __cpluscplus */

/* define marco */
#define  SYS_BOOT_VERSION            ("V0.01")

#ifndef SYS_HARD_VERSION
#define SYS_HARD_VERSION            ("010B300")
#endif

//Board Define
#define  SYS_XTAL_CLOCK_HZ          (32768U)
    
#define APPLICATION_ADDRESS         (0xC000)
#define VECTAB_LOAD_START           (APPLICATION_ADDRESS)
#define VECTAB_RELOAD_START         (0x0000)
#define SYS_DEV_TEST                (1)

#define SYS_ERASE_ADDR_START        (APPLICATION_ADDRESS)

/**
 * firmware information in spi flash
 */
#define SPI_FLASH_PAGE_SIZE             (256L)
#define SPI_FLASH_BLOCK_SIZE            (1024L * 4)
#define FIRMWARE_INFORMATION_ADDRESS    (0xF0000L)
#define FIRMWARE_ADDRESS_BASE           (FIRMWARE_INFORMATION_ADDRESS + SPI_FLASH_BLOCK_SIZE)
#define FIRMWARE_ADDRESS_END            (FIRMWARE_INFORMATION_ADDRESS + (1024L * 64))

#if defined (STM8L15X_MD) || defined (STM8L15X_MDP) || defined (STM8L15X_LD) || \
defined (STM8L05X_LD_VL) || defined (STM8L05X_MD_VL) || defined (STM8AL31_L_MD)
    /*!< Used with memory Models for code smaller than 64K */
    #define SYS_ERASE_ADDR_END      (0x00FFFF)
#elif defined (STM8L15X_HD) || defined (STM8L05X_HD_VL)
    /*!< Used with memory Models for code higher than 64K */
    #define SYS_ERASE_ADDR_END      (0x017FFF)
#else
    #error "Please select first the target STM8L device used in your application (in stm8l15x.h file)"
#endif

#define SYS_SPI_FIREWARE_INFO_ADDR  (0xFE000)
#define SYS_SPI_FIREWARE_NEW_ADDR   (0x000C0000)
#define SYS_SPI_FIREWARE_MAX_SIZE   (0x3800) //14 K 
#define SYS_YMODEM_TO_SPIFLASH      (0x01)

#ifdef __cpluscplus
}
#endif /* __cpluscplus */
#endif /* __OPTIONS_H__ */
