/**
 * /driver/printk.c
 *
 * Copyright (C) 2017 HinsShum
 *
 * printk.c is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*---------- includes ----------*/
#include "printk.h"
#include <stdarg.h>
#include <stdio.h>

/* global variable */
printk_struct_t g_printk_struct;

/* function */
#ifndef RELEASE_PUBLIC
uint16_t printk(const char *fmt, ...)
{
    va_list args;
    uint8_t buffer[128] = {0};
    uint16_t len = 0;

    va_start(args, fmt);
    vsnprintf((char *)buffer, sizeof(buffer), fmt, args);
    va_end(args);

    len = g_printk_struct.puts(buffer);
    return len;
}
#else 
uint16_t printk(const char *fmt, ...) { return 0; }
#endif
