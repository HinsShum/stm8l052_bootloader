/**
 * /driver/printk.h
 *
 * Copyright (C) 2017 HinsShum
 *
 * printk.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PRINTK_H
#define __PRINTK_H

#include <stm8l15x.h>

/* typedef */
typedef struct {
    uint16_t (*puts)(const uint8_t *);
} printk_struct_t;

/* extern */
extern printk_struct_t g_printk_struct;
extern uint16_t printk(const char *fmt, ...);

#endif /* __PRINTK_H */
