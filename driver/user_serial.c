/**
* @user_serial.c
*
* @copyright This file creat by rensuiyi ,all right reserve!
* 
* @author rensuyi
* 
* @date 2016/12/7 11:08:29
*/

/* include */
#include "bsp.h"
#include "bsp_usart.h"
#include "user_serial.h"
#include "platform.h"

/* global variable */
user_serial_t g_user_serial;

uint16_t user_serial_write(char *pdata, uint16_t len) {
    return g_user_serial.serial_write((uint8_t *)pdata, len);
}


bool user_serial_get_char(char *pout, uint16_t timeout_ms) {
    int16_t res;
    uint16_t timeout = 0;

    timeout = timeout_ms;

    do {
        res = simplefifo_read(&g_platform.uart_fifo, (uint8_t *)pout, 1);
        if (res == 1) {
            return TRUE;
        } else {
            bsp_delay_ms(1);
        }
    }while (--timeout);

    return FALSE;
}

bool user_serial_drop(void) {
    simplefifo_reset(&g_platform.uart_fifo);
    return TRUE;
}
