/**
 * @w25qxx.c
 *
 * @copyright This file creat by rensuiyi ,all right reserve!
 *
 * @author rensuyi
 *
 * @date 2015/4/30 9:45:50
 */
#include "w25qxx.h"

#ifndef  W25QXX_DEBUG_ENABLE
#define  W25QXX_DEBUG_ENABLE   (0)
#endif /* W25QXX_DEBUG_ENABLE */

#if W25QXX_DEBUG_ENABLE > 0
    #define __w25qxx_debug(x,y...)
#else
    #define __w25qxx_debug(x,y...)
#endif


#define W25QXX_STATUS_REG_BUSY  (1<<0)
#define W25QXX_STATUS_REG_WEL  (1<<1)

extern void bsp_delay_ms(uint16_t ms);
#define w25qxx_delay_ms(cnt)    bsp_delay_ms(cnt)

/* global variavle */
struct st_w25qxx_describe g_w25qxx;

/*
 * Get the Device ID,WinBond EF (0x13,0x14,0x15,0x16(8Mbytes))
 *
 * @author suiyi.key (2015/5/1)
 *
 * @param pw25qxx
 *
 * @return err_t
 */
static int32_t w25qxx_get_id(struct st_w25qxx_describe *pw25qxx) {
    uint8_t tmp = 0x00;

    pw25qxx->cs_ctl(TRUE);

    pw25qxx->xfer(0x90);
    pw25qxx->xfer(0x00);
    pw25qxx->xfer(0x00);
    pw25qxx->xfer(0x00);
    tmp = pw25qxx->xfer(0x00);
    pw25qxx->icode = ((uint16_t)tmp) << 8;
    tmp = pw25qxx->xfer(0x00);
    pw25qxx->icode = pw25qxx->icode + tmp;

    pw25qxx->cs_ctl(FALSE);

    return SL_EOK;
}

/*
 * Get the Status Register 1
 *
 * @author suiyi.key (2015/5/1)
 *
 * @param pw25qxx
 * @param status
 *
 * @return err_t
 */
static int32_t w25qxx_get_status(struct st_w25qxx_describe *pw25qxx, uint8_t *status) {
    pw25qxx->cs_ctl(TRUE);

    pw25qxx->xfer(0x05);
    *status = pw25qxx->xfer(0x00);

    pw25qxx->cs_ctl(FALSE);
    return SL_EOK;
}

/*
 * Enable Write,must be call before Write/Erase
 *
 * @author suiyi.key (2015/5/1)
 *
 * @param pw25qxx
 *
 * @return err_t
 */
static int32_t w25qxx_write_enable(struct st_w25qxx_describe *pw25qxx) {
    uint16_t wait_max_ticks = 100;
    uint8_t status = 0xff;

    pw25qxx->cs_ctl(TRUE);
    pw25qxx->xfer(0x06);
    pw25qxx->cs_ctl(FALSE);

    while(--wait_max_ticks > 0) {
        w25qxx_get_status(pw25qxx, &status);
        if(status & W25QXX_STATUS_REG_WEL) {
            break;
        }
        w25qxx_delay_ms(1);
    }
    if(wait_max_ticks > 0) {
        return SL_EOK;
    }
    __w25qxx_debug("write enable time out\r\n");
    return SL_E_TIME_OUT;

}

/*
 * erase the seckor  4K
 *
 * @author suiyi.key (2015/5/1)
 *
 * @param pw25qxx
 * @param addr
 *
 * @return err_t
 */
static int32_t w25qxx_erase_sector(struct st_w25qxx_describe *pw25qxx, uint32_t addr) {
    uint16_t wait_max_ms = 40;
    uint8_t status = 0xff;

    /*
     * write enable
     */
    if(SL_EOK != w25qxx_write_enable(pw25qxx)) {
        return SL_ERROR;
    }

    pw25qxx->cs_ctl(TRUE);
    pw25qxx->xfer(0x20);
    pw25qxx->xfer((addr >> 16) & 0xff);
    pw25qxx->xfer((addr >> 8) & 0xff);
    pw25qxx->xfer((addr >> 0) & 0xff);
    pw25qxx->cs_ctl(FALSE);
    w25qxx_delay_ms(1);
    /*
     * wait for ready 30-400ms
     */
    while(--wait_max_ms > 1) {
        w25qxx_get_status(pw25qxx, &status);
        if((status & W25QXX_STATUS_REG_BUSY) != 0) {
            break;
        }
        w25qxx_delay_ms(1);
    }
    //wait for ok
    wait_max_ms = 400;

    while(--wait_max_ms) {
        w25qxx_delay_ms(1);
        w25qxx_get_status(pw25qxx, &status);
        if((status & W25QXX_STATUS_REG_BUSY) == 0) {
            break;
        }

    }
    if(wait_max_ms > 0) {
        while(wait_max_ms-- > 340) {
            w25qxx_delay_ms(1);
        }
        return SL_EOK;
    }
    __w25qxx_debug("erase sector time out\r\n");
    return SL_E_TIME_OUT;
}

/*
 * erase the chip with in about 23s
 *
 * @author suiyi.key (2015/5/1)
 *
 * @param pw25qxx
 *
 * @return err_t
 */
static int32_t w25qxx_erase_chip(struct st_w25qxx_describe *pw25qxx) {
    uint8_t status;
    uint16_t wait_max_ms = 40000;

    /*
     * write enable
     */
    if(SL_EOK != w25qxx_write_enable(pw25qxx)) {
        return SL_ERROR;
    }

    w25qxx_delay_ms(1);
    pw25qxx->cs_ctl(TRUE);
    pw25qxx->xfer(0xc7);
    pw25qxx->cs_ctl(FALSE);
    w25qxx_delay_ms(1);
    /*
     * wait for ready
     */
    while(--wait_max_ms > 1) {

        w25qxx_get_status(pw25qxx, &status);
        if((status & W25QXX_STATUS_REG_BUSY) != 0) {
            break;
        }
        w25qxx_delay_ms(1);
    }
    while(--wait_max_ms) {
        w25qxx_delay_ms(1);
        w25qxx_get_status(pw25qxx, &status);
        if((status & W25QXX_STATUS_REG_BUSY) == 0) {
            break;
        }
    }

    if(wait_max_ms > 0) {
        //wait for ready
        w25qxx_delay_ms(10);
        return SL_EOK;
    }
    __w25qxx_debug("erase chip time out\r\n");
    return SL_E_TIME_OUT;
}

/*
 * Write data to a single page
 *
 * @author suiyi.key (2015/5/1)
 *
 * @param pw25qxx
 * @param addr
 * @param buf
 * @param len
 *
 * @return err_t
 */
static int32_t w25qxx_page_write(struct st_w25qxx_describe *pw25qxx, uint32_t addr, uint8_t *buf, uint16_t len) {
    //page algin
    uint16_t page_left = 0x00;
    uint16_t max_len = len;
    uint16_t write_len = 0;
    uint16_t wait_max_ms = 50;
    uint8_t status;
    int32_t res;
    if(addr > pw25qxx->addr_max) {
        __w25qxx_debug("addr:%08X not support\r\n", addr);
        return SL_ERROR;
    }
    page_left = pw25qxx->page_size - (addr % pw25qxx->page_size);
    if(page_left < max_len) {
        max_len = page_left;
    }

    write_len = max_len;

    wait_max_ms = 50;
    while(wait_max_ms-- > 1) {
        res = w25qxx_write_enable(pw25qxx);
        if(res == SL_EOK) {
            break;
        }
        w25qxx_delay_ms(1);
    }
    if(SL_EOK != res) {
        return SL_ERROR;
    }
    //start write

    pw25qxx->cs_ctl(TRUE);
    pw25qxx->xfer(0x02);
    pw25qxx->xfer((addr >> 16) & 0xff);
    pw25qxx->xfer((addr >> 8) & 0xff);
    pw25qxx->xfer((addr >> 0) & 0xff);

    while(max_len-- > 0) {
        pw25qxx->xfer(*buf++);
    }
    pw25qxx->cs_ctl(FALSE);

    wait_max_ms = 50;
    //wait for ready
    while(--wait_max_ms) {
        w25qxx_delay_ms(1);
        w25qxx_get_status(pw25qxx, &status);
        if((status & W25QXX_STATUS_REG_BUSY) == 0) {
            break;
        }

    }
    if(wait_max_ms > 0) {
        return write_len;
    }
    __w25qxx_debug("write time out\r\n");
    return SL_E_TIME_OUT;
}


static int32_t w25qxx_power_down(struct st_w25qxx_describe *pw25qxx) {
    pw25qxx->cs_ctl(TRUE);
    pw25qxx->xfer(0xB9);
    pw25qxx->cs_ctl(FALSE);
    w25qxx_delay_ms(1);

    return SL_EOK;
}

static int32_t w25qxx_power_up(struct st_w25qxx_describe *pw25qxx) {
    uint8_t tmp = 0x00;

    pw25qxx->cs_ctl(TRUE);

    pw25qxx->xfer(0xAB);
    pw25qxx->xfer(0x00);
    pw25qxx->xfer(0x00);
    pw25qxx->xfer(0x00);
    tmp = pw25qxx->xfer(0xFF);
    pw25qxx->cs_ctl(FALSE);

    if((tmp & 0xff) == (pw25qxx->icode & 0xFF)) {
        return SL_EOK;
    }

    return SL_ERROR;
}

/*
 * Get the Chip infomations
 *
 * @author suiyi.key (2015/4/30)
 *
 * @param pdev
 *
 * @return err_t
 */
int32_t w25qxx_init(struct st_w25qxx_describe *pw25qxx) {

    if(pw25qxx->xfer == NULL) {
        __w25qxx_debug("w25qxx driver get NULL parameters\r\n");
        return SL_E_POINT_NONE;
    }
    w25qxx_power_up(pw25qxx);
#if 1
    //get the id
    w25qxx_get_id(pw25qxx);
    //check if is w25qxx
    if((pw25qxx->icode >> 8) != 0xef) {
        __w25qxx_debug("flash type not pw25qxx:%04X\r\n", pw25qxx->icode);
        return SL_ERROR;
    }
    //check the sub type
    if((pw25qxx->icode & 0xff) == W25Q80_IDENTIFIED_CODE) {
        pw25qxx->page_size = 256;      //256byte
                                       //pw25qxx->sector_size = 4 * 1024; //4K
                                       //pw25qxx->sectors = 256;
                                       //pw25qxx->pages = 4096;
        pw25qxx->addr_max = 1 * 1024 * 1024ul; //1M
    }
    if((pw25qxx->icode & 0xff) == W25Q16_IDENTIFIED_CODE) {}
    if((pw25qxx->icode & 0xff) == W25Q64_IDENTIFIED_CODE) {
        pw25qxx->page_size = 256;      //256byte
                                       //pw25qxx->sector_size = 4 * 1024; //4K
                                       //pw25qxx->sectors = 2048;
                                       //pw25qxx->pages = 32768;
        pw25qxx->addr_max = 8 * 1024 * 1024ul; //8M
    }
#else
    pw25qxx->page_size = 256;      //256byte
    pw25qxx->sector_size = 4 * 1024; //4K
    pw25qxx->sectors = 256;
    pw25qxx->pages = 4096;
    pw25qxx->addr_max = 1 * 1024 * 1024ul; //1M
#endif
    __w25qxx_debug("w25qxx already initialized!");
    return SL_EOK;
}

int32_t w25qxx_read(struct st_w25qxx_describe *pw25qxx, void *buf, uint32_t addition, uint32_t len) {
    uint32_t max_len = len;
    uint8_t *pdata = (uint8_t *)buf;

    if((buf == NULL) || (pw25qxx->xfer == NULL)) {
        __w25qxx_debug("w25qxx driver get NULL parameters\r\n");
        return SL_E_POINT_NONE;
    }

    //read
    if(addition > pw25qxx->addr_max) {
        return 0;
    }
    max_len = pw25qxx->addr_max - addition;
    if(max_len > len) {
        max_len = len;
    }
    len = max_len;
    pw25qxx->cs_ctl(TRUE);
    pw25qxx->xfer(0x03);
    pw25qxx->xfer((addition >> 16) & 0xff);
    pw25qxx->xfer((addition >> 8) & 0xff);
    pw25qxx->xfer((addition >> 0) & 0xff);
    while(max_len-- > 0) {
        *pdata++ = pw25qxx->xfer(0x00);
    }
    pw25qxx->cs_ctl(FALSE);
    return len;
}

int32_t w25qxx_write(struct st_w25qxx_describe *pw25qxx, void *buf, uint32_t addition, uint32_t len) {
    uint16_t left_len = 0;
    uint32_t max_len = 0;
    uint32_t write_len = 0;
    int32_t res = 0;
    uint8_t *pdata = (uint8_t *)buf;

    if((buf == NULL) || (pw25qxx->xfer == NULL)) {
        __w25qxx_debug("w25qxx driver get NULL parameters\r\n");
        return SL_E_POINT_NONE;
    }
    if(addition > pw25qxx->addr_max) {
        return 0;
    }
    max_len = pw25qxx->addr_max - addition;
    if(max_len > len) {
        max_len = len;
    }

    //write
    left_len = pw25qxx->page_size - addition % pw25qxx->page_size;
    if(left_len > max_len) {
        left_len = max_len;
    }
    if(left_len > 0) {
        res = w25qxx_page_write(pw25qxx, addition, pdata, left_len);
        if(res < 0) {
            return res;
        }
        write_len += res;
        addition += left_len;
        max_len -= left_len;
    }

    while(max_len > 0) {
        if(max_len > pw25qxx->page_size) {
            res = w25qxx_page_write(pw25qxx, addition, pdata + write_len, pw25qxx->page_size);
            max_len -= pw25qxx->page_size;
            addition += pw25qxx->page_size;
        } else {
            res = w25qxx_page_write(pw25qxx, addition, pdata + write_len, max_len);
            max_len = 0x00;
        }
        if(res < 0) {
            return res;
        }
        write_len += res;
    }
    return write_len;
}

int32_t w25qxx_ioctl(struct st_w25qxx_describe *pw25qxx, uint32_t cmd, void *arg) {


    if(pw25qxx->xfer == NULL) {
        __w25qxx_debug("w25qxx driver get NULL parameters\r\n");
        return SL_E_POINT_NONE;
    }

    switch(cmd) {
        case IOCTL_DEVICE_POWER_OFF:
            if(pw25qxx->power_ctl) {
                w25qxx_delay_ms(20);
                pw25qxx->power_ctl(TRUE);
            }
            return SL_EOK;
        case IOCTL_DEVICE_POWER_ON:
            if(pw25qxx->power_ctl) {
                pw25qxx->power_ctl(FALSE);
                w25qxx_delay_ms(20);
            }
            return SL_EOK;
        case W25QXX_IOCTL_ERAES_CHIP:
            return w25qxx_erase_chip(pw25qxx);
        case W25QXX_IOCTL_SLEEP:
            pw25qxx->is_sleep = TRUE;
            return w25qxx_power_down(pw25qxx);
        case W25QXX_IOCTL_WAKE_UP:
            if(SL_EOK == w25qxx_power_up(pw25qxx)) {
                pw25qxx->is_sleep = FALSE;
                return SL_EOK;
            }
            return SL_ERROR;
        case W25QXX_IOCTL_ERAES_SEGMENT:
        {
            uint32_t *addr;
            if(arg == NULL) {
                return SL_E_POINT_NONE;
            }
            addr = (uint32_t *)arg;
            return w25qxx_erase_sector(pw25qxx, *addr);
        }
    }
    return SL_EOK;
}

