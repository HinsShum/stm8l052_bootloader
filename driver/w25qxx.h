/**
 * @file w25qxx.h
 *  
 * @brief this is the system hook file ,used to initial the user 
 *        task and user hardware
 *
 * @copyright This file creat by rensuiyi ,all right reserve!
 * 
 * @author rensuyi
 * 
 * @date 2015/4/20 10:42:55
 */
#ifndef __W25QXX_H__
#define __W25QXX_H__

#ifdef __cpluscplus
extern "c"
{
#endif /* __cpluscplus */

/* include */
#include <string.h>
#include "device.h"

#define  W25Q80_IDENTIFIED_CODE          (0x13)
#define  W25Q16_IDENTIFIED_CODE          (0x14)
#define  W25Q64_IDENTIFIED_CODE          (0x16)


#define  W25QXX_IOCTL_ERAES_SEGMENT       (IOCTL_USER_START + 0x01)
#define  W25QXX_IOCTL_ERAES_CHIP          (IOCTL_USER_START + 0x02)
#define  W25QXX_IOCTL_GET_ID              (IOCTL_USER_START + 0x03)
#define  W25QXX_IOCTL_SLEEP               (IOCTL_USER_START + 0x04)
#define  W25QXX_IOCTL_WAKE_UP             (IOCTL_USER_START + 0x05)


struct st_w25qxx_describe {
    uint8_t (*xfer)(uint8_t data);
    bool (*power_ctl)(bool on);
    bool (*cs_ctl)(bool on);
    uint16_t page_size;
    //uint32_t pages;
    //uint16_t sector_size;
    //uint32_t sectors;
    uint32_t addr_max;
    uint16_t icode;
    uint8_t  is_sleep;
};

extern struct st_w25qxx_describe g_w25qxx;;

extern int32_t w25qxx_init(struct st_w25qxx_describe *pw25qxx);
extern int32_t w25qxx_read(struct st_w25qxx_describe *pw25qxx, void *buf, uint32_t addition, uint32_t len);
extern int32_t w25qxx_write(struct st_w25qxx_describe *pw25qxx, void *buf, uint32_t addition, uint32_t len);
extern int32_t w25qxx_ioctl(struct st_w25qxx_describe *pw25qxx, uint32_t cmd, void *arg);
#ifdef __cpluscplus
}
#endif /* __cpluscplus */
#endif /* __W25QXX_H__ */
