/**
 * /src/main.c
 *
 * Copyright (C) 2017 HinsShum
 *
 * main.c is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*---------- includes ----------*/
#include "platform.h"
#include "errno.h"
#include "strategy.h"
#include "printk.h"
#include "firmware.h"
#include <string.h>

/* function */
int main(void)
{
    int32_t retval = FIRMWARE_UPDATE_OK;

    /* Initialize some of the necessary things for bootloader */
    platform_init();
    printk("bootloader\n");

    if(SL_EOK == strategy_check_serial()) {
        strategy_cmd_process();
    }
    if(g_platform.bin.magic_number == PLATFORM_BIN_MAGIC && g_platform.bin.bin_updated == FALSE) {
        /* read image from spi flash */
        retval = firmware_updating(g_platform.bin.bin_md5, strlen((char *)g_platform.bin.bin_md5), g_platform.bin.bin_size);
    }
    if(retval == FIRMWARE_UPDATE_OK) {
        printk("jump to application\n");
        /* check the entry */
        platform_reload_interrupt_vectortable();
    }
    /* if can not jump to app, than reset */
    printk("No application, system reset\n");
    platform_system_reset();
    while(TRUE);
}
