/**
 * /src/platform.c
 *
 * Copyright (C) 2017 HinsShum
 *
 * platform.c is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*---------- includes ----------*/
#include <stm8l15x.h>
#include <string.h>
#include <stdlib.h>
#include "stm8l15x_it.h"
#include "bsp.h"
#include "bsp_usart.h"
#include "bsp_flash.h"
#include "bsp_spiflash.h"
#include "platform.h"
#include "options.h"
#include "printk.h"
#include "w25qxx.h"
#include "sl_crc.h"

/* global variable */
struct st_platform_descibe g_platform;

/* static variable */
#if defined (STM8L15X_MD) || defined (STM8L15X_MDP) || defined (STM8L15X_LD) || \
defined (STM8L05X_LD_VL) || defined (STM8L05X_MD_VL) || defined (STM8AL31_L_MD)
#define UART_FIFO_BUF_LENGTH    (145)
static uint8_t m_uart_fifo_buf[150];
#elif defined (STM8L15X_HD) || defined (STM8L05X_HD_VL)
#define UART_FIFO_BUF_LENGTH    (1024)
static uint8_t m_uart_fifo_buf[1048];
#else
    #error "Please select first the target STM8L device used in your application (in stm8l15x.h file)"
#endif

/* function */
static char *int2str(long digit, char *str, long str_len)
{
    char *p = NULL;
    char number = 0;
    long digit_tmp = digit;

    str_len--;
    *(str + str_len) = '\0';
    if(digit < 0) {
        digit = -digit;
    }

    do {
        number = digit % 10;
        *(str + --str_len) = '0' + number;
        digit /= 10;
    } while(digit != 0 && str_len > 1);

    if(str_len > 0) {
        if(digit_tmp < 0) {
            str_len--;
            *(str + str_len) = '-';
        }
        p = str + str_len;
    }

    return p;
}

static void platform_load_app_info(void)
{
    struct st_platform_bin_header bin_info;
    uint8_t tmp_buf[10] = { 0 };
    uint8_t *p = NULL;
#if 0
    char *pmain_version = NULL;
    char *psub_version = NULL;
#endif
    memset(&bin_info, 0, sizeof(struct st_platform_bin_header));
    /* Get application information */
    w25qxx_read(&g_w25qxx, (void *)&bin_info, FIRMWARE_INFORMATION_ADDRESS, sizeof(struct st_platform_bin_header));

    if(bin_info.magic_number == PLATFORM_BIN_MAGIC) {
        /**
         * calc crc
         */
        if(0 != sl_revert_sum8((void *)&bin_info, sizeof(struct st_platform_bin_header))) {
            goto crc_err;
        }
        p = (uint8_t *)int2str(bin_info.bin_size, (char *)tmp_buf, sizeof(tmp_buf));
        strncpy((char *)g_platform.app.image_size, (char *)p, sizeof(g_platform.app.image_size) - 1);
        p = (uint8_t *)int2str(bin_info.bin_buildno, (char *)tmp_buf, sizeof(tmp_buf));
        strncpy((char *)g_platform.app.buildno, (char *)p, sizeof(g_platform.app.buildno) - 1);
        strncpy((char *)g_platform.app.image_md5, (char *)bin_info.bin_md5, sizeof(g_platform.app.image_md5) - 1);
        if(bin_info.bin_updated == TRUE) {
            strncpy((char *)g_platform.app.image_updated, "true", sizeof(g_platform.app.image_updated) - 1);
        } else {
            strncpy((char *)g_platform.app.image_updated, "false", sizeof(g_platform.app.image_updated) - 1);
        }
        strncpy((char *)g_platform.app.model_hw, (char *)bin_info.model_hw, sizeof(g_platform.app.model_hw) - 1);
        strncpy((char *)g_platform.app.model_fun, (char *)bin_info.model_fun, sizeof(g_platform.app.model_fun) - 1);

        g_platform.bin = bin_info;
    } else {
crc_err:
        strncpy((char *)g_platform.app.image_md5, "UNKOWN", sizeof(g_platform.app.image_md5) - 1);
        strncpy((char *)g_platform.app.image_size, "UNKONW", sizeof(g_platform.app.image_size) - 1);
        strncpy((char *)g_platform.app.buildno, "?", sizeof(g_platform.app.buildno) - 1);
        strncpy((char *)g_platform.app.image_updated, "UNKOWN", sizeof(g_platform.app.image_updated) - 1);
        strncpy((char *)g_platform.app.model_hw, "UNKOWN", sizeof(g_platform.app.model_hw) - 1);
        strncpy((char *)g_platform.app.model_fun, "UNKOWN", sizeof(g_platform.app.model_fun) - 1);
    }
}

static void platform_peripherals_init(void)
{
    sim();  /* disable interrupt */
    sysclock_init_div1();
    bsp_usart_init();
    bsp_flash_init();
    bsp_spiflash_init();
    rim();  /* enable interrupt */
}

static void platform_peripherals_deinit(void)
{
    bsp_usart_deinit();
    bsp_flash_deinit();
}

static void platform_driver_init(void)
{
    if(SL_EOK != w25qxx_init(&g_w25qxx)) {
        printk("SPIFlash init failed, program stop!\n");
        while(TRUE);
    }
}

static void platform_misc_init(void)
{
    platform_load_app_info();
    /* fifo for ymodem initlization */
    simplefifo_init(&g_platform.uart_fifo, m_uart_fifo_buf, UART_FIFO_BUF_LENGTH);
}

void platform_system_reset(void)
{
    bsp_system_reset();
}

void platform_reload_interrupt_vectortable(void)
{
    uint8_t *check = (uint8_t *)APPLICATION_ADDRESS;

    if(*check == 0x82 && *(check + 4) == 0x82) {
        uint8_t *src = (uint8_t *)APPLICATION_ADDRESS;
        uint8_t *dst = (uint8_t *)VECTAB_RELOAD_START;
        uint16_t cnt = sizeof(isr_handler);

        /* disable interrupt, interrupt will be enable in application */
        sim();
        platform_peripherals_deinit();
        /* reload interrupt vector table(application) from flash to memory */
        for(; cnt > 0; cnt--) {
            *dst++ = *src++;
        }

        /* reset stack pointer (lower byte - because compiler decreases SP with some bytes) */
        asm("LDW X,  SP ");
        asm("LD  A,  $FF");
        asm("LD  XL, A  ");
        asm("LDW SP, X  ");
        asm("JPF $C000  "); /* APPLICATION_ADDRESS */
    }
}

void platform_init(void)
{
    platform_peripherals_init();
    platform_driver_init();
    platform_misc_init();
}

