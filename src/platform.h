/**
 * /src/platform.h
 *
 * Copyright (C) 2017 HinsShum
 *
 * platform.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __PLATFORM_H
#define __PLATFORM_H

/* include */
#include <stm8l15x.h>
#include "simplefifo.h"

/* define marco */
#define PLATFORM_BIN_MAGIC  (0x5A5A)

/* define struct */
struct st_platform_soft_info {
    int8_t image_size[8];
    int8_t buildno[4];
    int8_t image_md5[32 + 1];
    int8_t image_updated[7];
    uint8_t model_hw[8 + 1];
    uint8_t model_fun[8 + 1];
};

struct st_platform_bin_header {
    uint8_t crc_sum;
    uint8_t reserve[3];
    uint32_t bin_buildno;
    uint32_t bin_size;
    uint8_t bin_md5[32 + 1];
    bool bin_updated;
    uint16_t magic_number;
    uint8_t model_hw[8 + 1];
    uint8_t model_fun[8 + 1];
};

struct st_platform_descibe{
    struct st_platform_bin_header bin;
    struct st_platform_soft_info app;
    struct st_simplefifo_describe uart_fifo;
#if defined (STM8L15X_MD) || defined (STM8L15X_MDP) || defined (STM8L15X_LD) || \
defined (STM8L05X_LD_VL) || defined (STM8L05X_MD_VL) || defined (STM8AL31_L_MD)
    /* RAM only 2K */
    char gbuf[150];
#elif defined (STM8L15X_HD) || defined (STM8L05X_HD_VL)
    /* RAM 4K */
    char gbuf[1200];
#else
    #error "Please select first the target STM8L device used in your application (in stm8l15x.h file)"
#endif
    uint8_t flash_ok;
#if 0
    uint8_t app_main_version;
    uint8_t app_sub_version;
#endif
};

/* prototype */
extern struct st_platform_descibe g_platform;

extern void platform_init(void);
extern void platform_system_reset(void);
extern void platform_reload_interrupt_vectortable(void);

#endif /* __PLATFORM_H */
