/**************************************************
 *
 * System initialization code for the STM8 IAR Compiler.
 *
 * Copyright 2010 IAR Systems AB.
 *
 * $Revision: 1413 $
 *
 ***************************************************
 *
 * To add your own interrupt handler to the table,
 * give it the label _interrupt_N, where N is the
 * offset from the RESET vector.  Your label will
 * override the corresponding weak label declaration
 * on the unhandled_exception function.
 *
 **************************************************/

        MODULE   ?interrupt

        SECTION __DEFAULT_CODE_SECTION__:CODE:NOROOT

declare_label MACRO
        PUBWEAK _interrupt_\1
_interrupt_\1:
        ENDM

unhandled_exception:

        declare_label 1
        declare_label 2
        declare_label 3
        declare_label 4
        declare_label 5
        declare_label 6
        declare_label 7
        declare_label 8
        declare_label 9
        declare_label 10
        declare_label 11
        declare_label 12
        declare_label 13
        declare_label 14
        declare_label 15
        declare_label 16
        declare_label 17
        declare_label 18
        declare_label 19
        declare_label 20
        declare_label 21
        declare_label 22
        declare_label 23
        declare_label 24
        declare_label 25
        declare_label 26
        declare_label 27
        declare_label 28
        declare_label 29
        declare_label 30
        declare_label 31

        NOP                                   ;; put a breakpoint here
        JRA    unhandled_exception


/*
 * The interrupt vector table.
 */


        SECTION `.intvec`:CONST

define_vector MACRO
        DC8     0x82
        DC24    _interrupt_\1
        ENDM

        PUBLIC  __intvec
        EXTERN   __iar_program_start
        


__intvec:
        DC8     0x82
        DC24    __iar_program_start          ;; RESET    0x8000
        DC8     0x82
        DC24    0x0004
        DC8     0x82
        DC24    0x0008
        DC8     0x82
        DC24    0x000C
        DC8     0x82
        DC24    0x0010
        DC8     0x82
        DC24    0x0014
        DC8     0x82
        DC24    0x0018
        DC8     0x82
        DC24    0x001C
        DC8     0x82
        DC24    0x0020
        DC8     0x82
        DC24    0x0024
        DC8     0x82
        DC24    0x0028
        DC8     0x82
        DC24    0x002C
        DC8     0x82
        DC24    0x0030
        DC8     0x82
        DC24    0x0034
        DC8     0x82
        DC24    0x0038
        DC8     0x82
        DC24    0x003C
        DC8     0x82
        DC24    0x0040
        DC8     0x82
        DC24    0x0044
        DC8     0x82
        DC24    0x0048
        DC8     0x82
        DC24    0x004C
        DC8     0x82
        DC24    0x0050
        DC8     0x82
        DC24    0x0054
        DC8     0x82
        DC24    0x0058
        DC8     0x82
        DC24    0x005C
        DC8     0x82
        DC24    0x0060
        DC8     0x82
        DC24    0x0064
        DC8     0x82
        DC24    0x0068
        DC8     0x82
        DC24    0x006C
        DC8     0x82
        DC24    0x0070
        DC8     0x82
        DC24    0x0074
        DC8     0x82
        DC24    0x0078
        DC8     0x82
        DC24    0x007C

        END
