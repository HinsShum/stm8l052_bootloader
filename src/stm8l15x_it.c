/**
  ******************************************************************************
  * @file    Project/STM8L15x_StdPeriph_Template/stm8l15x_it.c
  * @author  MCD Application Team
  * @version V1.6.1
  * @date    30-September-2014
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_it.h"

/** @addtogroup STM8L15x_StdPeriph_Template
  * @{
  */
/* Private function prototypes -----------------------------------------------*/
extern INTERRUPT void __iar_program_start(void);  /* IAR startup function */
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
struct interrupt_vector isr_handler[32] @".memvectab" = {
    {0x82, 0x00, __iar_program_start},
    {0x82, 0x00, TRAP_IRQHandler},
    {0x82, 0x00, NMI_IRQHandler},
    {0x82, 0x00, FLASH_IRQHandler},
    {0x82, 0x00, DMA1_CHANNEL0_1_IRQHandler},
    {0x82, 0x00, DMA1_CHANNEL2_3_IRQHandler},
    {0x82, 0x00, RTC_CSSLSE_IRQHandler},
    {0x82, 0x00, EXTIE_F_PVD_IRQHandler},
    {0x82, 0x00, EXTIB_G_IRQHandler},
    {0x82, 0x00, EXTID_H_IRQHandler},
    {0x82, 0x00, EXTI0_IRQHandler},
    {0x82, 0x00, EXTI1_IRQHandler},
    {0x82, 0x00, EXTI2_IRQHandler},
    {0x82, 0x00, EXTI3_IRQHandler},
    {0x82, 0x00, EXTI4_IRQHandler},
    {0x82, 0x00, EXTI5_IRQHandler},
    {0x82, 0x00, EXTI6_IRQHandler},
    {0x82, 0x00, EXTI7_IRQHandler},
    {0x82, 0x00, LCD_AES_IRQHandler},
    {0x82, 0x00, SWITCH_CSS_BREAK_DAC_IRQHandler},
    {0x82, 0x00, ADC1_COMP_IRQHandler},
    {0x82, 0x00, TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler},
    {0x82, 0x00, TIM2_CC_USART2_RX_IRQHandler},
    {0x82, 0x00, TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler},
    {0x82, 0x00, TIM3_CC_USART3_RX_IRQHandler},
    {0x82, 0x00, TIM1_UPD_OVF_TRG_COM_IRQHandler},
    {0x82, 0x00, TIM1_CC_IRQHandler},
    {0x82, 0x00, TIM4_UPD_OVF_TRG_IRQHandler},
    {0x82, 0x00, SPI1_IRQHandler},
    {0x82, 0x00, USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler},
    {0x82, 0x00, USART1_RX_TIM5_CC_IRQHandler},
    {0x82, 0x00, I2C1_SPI2_IRQHandler}
};
/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/
/**
  * @brief TRAP interrupt routine
  * @par Parameters:
  * None
  * @retval 
  * None
*/
INTERRUPT void TRAP_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief NMI interrupt routine
  * @par Parameters:
  * None
  * @retval 
  * None
*/
INTERRUPT void NMI_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief FLASH Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void FLASH_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief DMA1 channel0 and channel1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void DMA1_CHANNEL0_1_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief DMA1 channel2 and channel3 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void DMA1_CHANNEL2_3_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief RTC / CSS_LSE Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void RTC_CSSLSE_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief External IT PORTE/F and PVD Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTIE_F_PVD_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PORTB / PORTG Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTIB_G_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PORTD /PORTH Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTID_H_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN0 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTI0_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTI1_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN2 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTI2_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN3 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTI3_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN4 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTI4_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN5 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTI5_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN6 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTI6_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief External IT PIN7 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void EXTI7_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief LCD /AES Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void LCD_AES_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief CLK switch/CSS/TIM1 break Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void SWITCH_CSS_BREAK_DAC_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief ADC1/Comparator Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void ADC1_COMP_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief TIM2 Update/Overflow/Trigger/Break /USART2 TX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief Timer2 Capture/Compare / USART2 RX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void TIM2_CC_USART2_RX_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}


/**
  * @brief Timer3 Update/Overflow/Trigger/Break Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief Timer3 Capture/Compare /USART3 RX Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void TIM3_CC_USART3_RX_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief TIM1 Update/Overflow/Trigger/Commutation Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void TIM1_UPD_OVF_TRG_COM_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief TIM1 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void TIM1_CC_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief TIM4 Update/Overflow/Trigger Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void TIM4_UPD_OVF_TRG_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @brief SPI1 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void SPI1_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */		
}

/**
  * @brief USART1 TX / TIM5 Update/Overflow/Trigger/Break Interrupt  routine.
  * @param  None
  * @retval None
  */
INTERRUPT void USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}

/**
  * @brief USART1 RX / Timer5 Capture/Compare Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void USART1_RX_TIM5_CC_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
    uint8_t ch = 0;
    extern bool isr_usart1_rx_handler(uint8_t, uint32_t);

    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
      ch = USART_ReceiveData8(USART1);
      isr_usart1_rx_handler(ch, (uint32_t)USART1_RX_TIM5_CC_IRQHandler);
      /* do not clear the RXNE pending bit
       * RXNE pending bit can be also cleared by a read to the USART_DR register
       * (USART_ReceiveData8() or USART_ReceiveData9()).
       */
    }
}

/**
  * @brief I2C1 / SPI2 Interrupt routine.
  * @param  None
  * @retval None
  */
INTERRUPT void I2C1_SPI2_IRQHandler(void)
{
    /* In order to detect unexpected events during development,
       it is recommended to set a breakpoint on the following instruction.
    */
}
/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
