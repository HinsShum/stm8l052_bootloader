#ifndef __MD5_H__
#define __MD5_H__

struct st_md5_ctx
{
	unsigned long count[2];
	unsigned long state[4];
	unsigned char buffer[64];   
};
                                          
void md5_init(struct st_md5_ctx *context);
void md5_update(struct st_md5_ctx *context, unsigned char *input, unsigned long inputlen);
void md5_final(struct st_md5_ctx *context, unsigned char *digest);

#endif  /* __MD5_H__ */
